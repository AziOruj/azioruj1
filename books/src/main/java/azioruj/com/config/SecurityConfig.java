package azioruj.com.config;

import azioruj.com.security.LoggingFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
@Configuration
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {
    private final JWTConfig jwtConfig;
    private final LoggingConfig loggingConfig;
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        http
                .csrf()
                .disable()
                .authorizeRequests()
                .antMatchers(HttpMethod.POST,"/books").hasAnyRole("PUBLISHER")
                .antMatchers(HttpMethod.PUT,"/books/*").hasAnyRole("PUBLISHER")
                .antMatchers(HttpMethod.DELETE,"/books/*").hasAnyRole("PUBLISHER")
                .anyRequest()
                .authenticated();
        http.apply(jwtConfig);
        http.apply(loggingConfig);
    }
}
