package azioruj.com.config;

import azioruj.com.security.JWTFilter;
import azioruj.com.security.LoggingFilter;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;


@Configuration
@RequiredArgsConstructor
public class LoggingConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {
    private final LoggingFilter loggingFilter;
    @Override
    public void configure(HttpSecurity builder) throws Exception {
        builder.addFilterBefore(loggingFilter, JWTFilter.class);
    }



}
