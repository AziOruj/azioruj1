package azioruj.com.jwt;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JWTService {
    private final Key key;

    public JWTService() {
        String keyStr = "U0hKREdGU0dIRFMqXzMyNDMyNDMyd2R3ZSlTSEpER0ZTR0hEUypfMzI0MzI0MzJ3ZHdlKSBTSEpER0ZTR0hEUypfMzI0MzI0MzJ3ZHdlKQ==";
        byte[] keyByte = Decoders.BASE64.decode(keyStr);
        key = Keys.hmacShaKeyFor(keyByte);
    }


    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
}


