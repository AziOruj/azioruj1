package azioruj.com.controller;

import azioruj.com.entity.dto.BookCreateDto;
import azioruj.com.entity.dto.BookDto;
import azioruj.com.exception.BookNotFoundException;
import azioruj.com.service.BookService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@RestController
@Slf4j
@RequiredArgsConstructor
@RequestMapping("/books")
public class BookController {
    private final BookService bookService;
    @PostMapping
    public ResponseEntity<BookDto> createBook(@RequestBody @Validated BookCreateDto createBook, Principal principal){
        return ResponseEntity.ok(bookService.createBook(createBook,principal));
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasPermission(#id, 'Foo', 'read')")
    public ResponseEntity<BookDto> updateBook(@RequestBody @Validated BookCreateDto updateBook, @PathVariable Long id){
      log.info("update");
         return ResponseEntity.ok(bookService.updateBook(updateBook,id));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BookDto> getBook(@PathVariable Long id){

       try {
           return ResponseEntity.ok(bookService.getBook(id));
       }catch (BookNotFoundException e){
           return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
       }
    }
    @PreAuthorize("hasPermission(#id, 'Foo', 'read')")
    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteBook(@PathVariable Long id){
        bookService.deleteBook(id);
        return ResponseEntity.ok().build();
    }


}
