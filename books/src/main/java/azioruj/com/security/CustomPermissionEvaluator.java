package azioruj.com.security;

import azioruj.com.exception.BookNotFoundException;
import azioruj.com.repository.BookRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import java.io.Serializable;

@Slf4j
@Component
public class CustomPermissionEvaluator implements PermissionEvaluator {
    @Autowired
    private  BookRepository bookRepository;

    @Override
    public boolean hasPermission(Authentication authentication, Object targetDomainObject, Object permission) {
        return false;
    }
    @Override
    public boolean hasPermission(Authentication authentication, Serializable serializable, String s, Object o) {
        log.info("Has permission in action {}  {} {}", serializable, s, o);
       try {
           String author = bookRepository.findById(Long.parseLong(serializable.toString()))
                   .orElseThrow(BookNotFoundException::new).getAuthor();
           return author.equalsIgnoreCase(authentication.getName());
       } catch (BookNotFoundException e){
           return false;
       }
    }





}

