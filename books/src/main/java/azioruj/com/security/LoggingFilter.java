package azioruj.com.security;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Component;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
@Order(1)
@Slf4j
public class LoggingFilter implements Filter {
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        log.info("Filter init {}",filterConfig);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        String remoteUser = ((HttpServletRequest) request).getRemoteUser();
        log.info("remoteUser {}",remoteUser);
        if (remoteUser==null){
            ((HttpServletResponse) response).setStatus(401);
    }
     chain.doFilter(request,response);
    }

    @Override
    public void destroy() {
        log.info("Filter destroy");
    }
}