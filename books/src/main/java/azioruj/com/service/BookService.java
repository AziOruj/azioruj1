package azioruj.com.service;

import azioruj.com.entity.dto.BookCreateDto;
import azioruj.com.entity.dto.BookDto;

import java.security.Principal;

public interface BookService {
    BookDto createBook(BookCreateDto create, Principal principal);
    BookDto updateBook(BookCreateDto update, Long id);
    BookDto getBook(Long id);
    void deleteBook(Long id);
}
