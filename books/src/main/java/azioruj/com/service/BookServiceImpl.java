package azioruj.com.service;

import azioruj.com.entity.Books;
import azioruj.com.entity.dto.BookCreateDto;
import azioruj.com.entity.dto.BookDto;
import azioruj.com.exception.BookNotFoundException;
import azioruj.com.repository.BookRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;

@Service
@Slf4j
@RequiredArgsConstructor
public class BookServiceImpl implements BookService{
    private final BookRepository bookRepository;
    private final ModelMapper mapper;

    @Override
    public BookDto createBook(BookCreateDto create, Principal principal) {
        Books book =mapper.map(create,Books.class);
        book.setAuthor(principal.getName());
        log.info("Author is {}",principal.getName());
        return mapper.map(bookRepository.save(book),BookDto.class);
    }

    @Override
    @Transactional
    public BookDto updateBook(BookCreateDto update, Long id) {
        Books book = bookRepository.findById(id).orElseThrow(BookNotFoundException::new);
        book.setBookName(update.getBookName());
        return mapper.map(book,BookDto.class) ;
    }

    @Override
    public BookDto getBook(Long id) {
        return mapper.map(bookRepository.findById(id).
                orElseThrow(BookNotFoundException::new)
                ,BookDto.class);
    }

    @Override
    public void deleteBook(Long id) {
         if (bookRepository.existsById(id)){
             bookRepository.deleteById(id);
         }
         else{
             throw new BookNotFoundException();
         }
    }
}
