package azioruj.com.entity.dto;

import lombok.Data;
import org.springframework.lang.Nullable;

@Data
public class BookCreateDto {
    @Nullable
    private String bookName;
}
