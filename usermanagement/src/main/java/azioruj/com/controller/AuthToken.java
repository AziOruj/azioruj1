package azioruj.com.controller;

import azioruj.com.controller.dto.TokenDto;
import azioruj.com.jwt.JWTService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class AuthToken {
    private final JWTService jwtService;
    @GetMapping("/token")
    public TokenDto createToken(Authentication authentication){
        return new TokenDto(jwtService.issueToken(authentication));
    }
}
