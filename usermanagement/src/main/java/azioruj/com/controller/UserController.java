package azioruj.com.controller;

import azioruj.com.entity.dto.UserDto;
import azioruj.com.entity.dto.UserDtoCreate;
import azioruj.com.exception.UserNotFoundException;
import azioruj.com.exception.UsernameAlreadyExistsException;
import azioruj.com.service.UsersService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@Slf4j
@RequestMapping("/users")
public class UserController {
    private final UsersService usersService;
    @PostMapping("/sign-up")
    public ResponseEntity<UserDto> createUser(@RequestBody UserDtoCreate create){
        log.info("Create user for controller {}",create);
       try {
           return ResponseEntity.status(HttpStatus.CREATED).body(usersService.create(create));
       }catch (UsernameAlreadyExistsException e){
           return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
       }

    }
    @PutMapping("/{id}")
    public ResponseEntity<UserDto> updateUser(@RequestBody UserDtoCreate update, @PathVariable Long id) {
        try {
            return ResponseEntity.ok().body(usersService.update(update, id));
        } catch (UsernameAlreadyExistsException | UserNotFoundException exception) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }
    @GetMapping("/{id}")
    public ResponseEntity<UserDto> getUser(@PathVariable Long id){
        log.info("Create user for controller id {}",id);
       try {
           return ResponseEntity.ok().body(usersService.get(id));
       }catch (UserNotFoundException e) {
           return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
       }
    }
    @DeleteMapping("{id}")
    public void deleteUser(@PathVariable Long id){
        usersService.delete(id);
    }
}
