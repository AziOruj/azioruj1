package azioruj.com.jwt;


import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Service;

import java.security.Key;
import java.time.Instant;
import java.util.Date;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class JWTService {
    private final static long duration=60*1000;
    private final Key key;

    public JWTService() {
        String keyStr = "U0hKREdGU0dIRFMqXzMyNDMyNDMyd2R3ZSlTSEpER0ZTR0hEUypfMzI0MzI0MzJ3ZHdlKSBTSEpER0ZTR0hEUypfMzI0MzI0MzJ3ZHdlKQ==";
        byte[] keyByte = Decoders.BASE64.decode(keyStr);
        key = Keys.hmacShaKeyFor(keyByte);
    }

    public String issueToken(Authentication authentication) {


        return Jwts.builder()
                .setSubject(authentication.getName())
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(Date.from(Instant.now().plusSeconds(duration)))
                .setHeader(Map.of("type", "JWT"))
                .signWith(key, SignatureAlgorithm.HS512)
                .claim("roles", getRoles(authentication))
                .compact();
    }
    private Set<String> getRoles(Authentication authentication){
        return authentication.getAuthorities().stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
    }
    public Claims parseToken(String token){
        return Jwts.parserBuilder()
                .setSigningKey(key)
                .build()
                .parseClaimsJws(token)
                .getBody();
    }
}


