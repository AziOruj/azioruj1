package azioruj.com.service;

import azioruj.com.entity.User;
import azioruj.com.entity.dto.UserDto;
import azioruj.com.entity.dto.UserDtoCreate;
import azioruj.com.exception.UserNotFoundException;
import azioruj.com.exception.UsernameAlreadyExistsException;
import azioruj.com.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Slf4j
@RequiredArgsConstructor
public class UserServiceImpl implements UsersService{

    private final UserRepository userRepository;
    private  final ModelMapper modelMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public UserDto create(UserDtoCreate create) {
        log.info("Create user for controller {}",create);
        if (!userExists(create.getUsername())) {
            User user = modelMapper.map(create, User.class);
            user.setPassword(passwordEncoder.encode(create.getPassword()));
            return modelMapper.map(userRepository.save(user), UserDto.class);
        }
        throw new UsernameAlreadyExistsException();
    }

    @Override
    public UserDto update(UserDtoCreate update,Long id) {
        if (userExists(update.getUsername())) {
            User user = modelMapper.map(update,User.class);
            user.setPassword(passwordEncoder.encode(update.getPassword()));
            user.setId(userRepository.findById(id).orElseThrow(UserNotFoundException::new).getId());
            return modelMapper.map(userRepository.save(user),UserDto.class);
        }
        throw new UsernameAlreadyExistsException();
    }

    @Override
    @Transactional
    public UserDto get(Long id) {
        return modelMapper.map(userRepository.
                findById(id).orElseThrow(UserNotFoundException::new),UserDto.class);
    }

    @Override
    public void delete(Long id) {
           if (userHas(id)){
               userRepository.deleteById(id);
           }else
               throw new UserNotFoundException();

    }
    private Boolean userExists(String username){
        return userRepository.existsByUsername(username);
    }
    private Boolean userHas(Long id){
        return userRepository.existsById(id);
    }
}
