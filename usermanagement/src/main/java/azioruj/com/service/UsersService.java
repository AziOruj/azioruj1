package azioruj.com.service;

import azioruj.com.entity.dto.UserDto;
import azioruj.com.entity.dto.UserDtoCreate;

import java.util.List;

public interface UsersService {
    UserDto create(UserDtoCreate create);
    UserDto update(UserDtoCreate update, Long id);
    UserDto get(Long id);
    void delete(Long id);
}
