package azioruj.com.repository;

import azioruj.com.entity.User;

import org.springframework.data.jpa.repository.EntityGraph;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Lock;

import javax.persistence.LockModeType;
import java.util.Optional;

public interface UserRepository extends JpaRepository<User,Long> {
    @EntityGraph(attributePaths = "authorities")
    Optional<User> findByUsername(String username);
    @Lock(LockModeType.OPTIMISTIC)
    Boolean existsByUsername(String username);
}
