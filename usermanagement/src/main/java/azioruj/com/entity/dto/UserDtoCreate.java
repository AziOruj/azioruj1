package azioruj.com.entity.dto;

import azioruj.com.entity.Authority;
import lombok.Data;
import org.springframework.lang.Nullable;

import java.util.HashSet;
import java.util.Set;

@Data
public class UserDtoCreate {
    @Nullable
    private String password;
    @Nullable
    private String username;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private boolean credentialsNonExpired;
    private boolean enabled;
    @Nullable
    private Set<Authority> authorities = new HashSet<>();
}
