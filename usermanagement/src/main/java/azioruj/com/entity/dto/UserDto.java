package azioruj.com.entity.dto;


import azioruj.com.entity.Authority;
import lombok.Data;

import java.util.HashSet;
import java.util.Set;

@Data
public class UserDto {

    private Long id;
    private String username;
}
